import java.util.*;
import java.lang.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws java.lang.Exception {

        Scanner input = new Scanner(System.in);
        int t = input.nextInt(); // number of sets to calculate


        for (int i = 0; i < t; i++) {
            int n = input.nextInt(); // how many numbers to calculate average from
            int[] oceny = new int[n];

            boolean doable = true;

            for (int j = 0; j < n; j++) {
                int x = input.nextInt(); // reading the marks
                oceny[j] = x;
                if (x < 4) {
                    doable = false; // assume doable... if all = 4 it doable, if any mark is less than 4 then check again if there's 5 or 6 in set
                }
            }

            for (int ocena : oceny) { // checking again if doable
                if (ocena >= 5) {
                    doable = true;
                    break;
                }
            }

            int sumOcenaDoKtej = 0;
            double wynik = 0;
            int k = 0;
            if (doable) {

                do {
                    k++;
                    sumOcenaDoKtej = 0;
                    for (int ocena : oceny) {
                        sumOcenaDoKtej += Math.pow(ocena, k);
                    }
                    wynik = Math.pow((double) sumOcenaDoKtej / n, 1.0 / k);
                }
                while ((int) wynik < 4);

                System.out.println(k);
            } else {
                System.out.println(-1);
            }

        }
    }
}



/*
TODO: napisac testy :-D

SKO - Średnia ocen
        Jaś ma nie za ciekawą sytuacje z języka polskiego , wie że Pani liczy średnią z ocen.
         Spytał się Pani czy mogła by policzyć średnią kwadratową ,
         Pani niepewnie ale się zgodziła , jednak zastrzegła fakt że to co wyjdzie z średnie zaokrągla
          w dół (jeśli wyjdzie 3.99 to wystawi 3 jeśli zaś wyjdzie 4 to wystawi 4).
           Jaś obliczył sobie średnią kwadratową i nadal nie jest zadowolony z tego wyniku.
           Więc spytał się czy może być średnia do potęgi k-tej a nie kwadratowej .
           Pani niepewnie się zgodziła , myśli że ma to mały wpływ na końcowy wynik.
           Jaś jednak chce wybrać jak najmniejsze k by Pani się nie poznała jakie
            znaczenie ma liczba k (k powinno być najmniejszą naturalną liczbą).

        Średnia=k sqrt((a1k+a2k+a3k+...+ank)/n)





W pierwszej linijce jest liczba t-liczba zestawów.                                0<t<100001
W każdym zestawie na początku jest liczba n-liczba ocen                  0<n<1001
Następnie jest n ocen                                                                     1<=ai<=6


Output
Należy dla każdego zestawu wypisać najmniejszą naturalną liczbę k dla której Jasiowi wyjdzie 4.
Zaś jeśli nie jest to możliwe dla k naturalnego należy wypisać -1



Input:
4
5
1 2 3 1 3
4
1 2 3 5
5
1 1 5 1 1
2
4 2

Output:
-1
6
8
-1
*/